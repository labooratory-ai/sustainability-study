# Deep Learning Implementation in Pathology: Modeling Operational Greenhouse Gas Emissions

![Greenhouse Gas Emissions](https://img.shields.io/badge/GHG%20Emissions-%20Modeling%20and%20Analysis-brightgreen)

## Introduction

Welcome to the repository for our sustainability project on "Modeling Operational Greenhouse Gas Emissions of Deep Learning Implementation in Pathology." This project aims to analyze the carbon footprint of deep learning (DL) models used in pathology and assess their potential environmental impact.

## Paper Title

The title of our paper is "Modeling Operational Greenhouse Gas Emissions of Deep Learning Implementation in Pathology."

## Reproducibility

To reproduce all the results and findings presented in the paper, please use the provided Jupyter notebook "Sustainability.ipynb." The notebook contains the necessary scripts and data to run the calculations and analyses and make the figures.

## Data Availability

All the necessary data files are included in the "data.zip" archive. 

## CO2eq Emission Calculation

The CO2eq emission calculations for different pathology tasks are performed using the following scripts:
- "classification.py": For classification tasks
- "reading.py": For reading tasks
- "segmentation.py": For segmentation tasks

## Citation

If you find our work useful, we would appreciate it if you could cite our paper:

```
[citation]
```

## License

This project is licensed under the MIT License.

## Contact

If you have any questions or need further information, please feel free to contact us at [vafaei.sadr@gmail.com](mailto:vafaei.sadr@gmail.com).

Thank you for your interest in our sustainability project!
