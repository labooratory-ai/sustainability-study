#!/usr/bin/env python
# coding: utf-8
from pynvml import *
nvmlInit()

free_device = -1
deviceCount = nvmlDeviceGetCount()
print('Available Devices')
for i in range(deviceCount):
    handle = nvmlDeviceGetHandleByIndex(i)
    info = nvmlDeviceGetMemoryInfo(handle)
    used = info.used/1024/1024
    if used<1024:
        print("Device", i, ":", nvmlDeviceGetName(handle))
        free_device = i
print('Device '+str(free_device)+' is selected.')
import os
os.environ["CUDA_VISIBLE_DEVICES"]=str(free_device)

import numpy as np
from os.path import exists
from tensorflow import keras
from tensorflow.keras import layers
from sys import argv
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import applications
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from functools import partial
from albumentations import (
    Compose, JpegCompression, HueSaturationValue, HorizontalFlip,
    Rotate,ImageCompression,RandomBrightnessContrast
)
from experiment_impact_tracker.compute_tracker import ImpactTracker
AUTOTUNE = tf.data.experimental.AUTOTUNE

# for i,j in enumerate([i for i in dir(applications) if i[0].isupper()]):
#     print(i,j)',
# 0: 'DenseNet121',
# 1: 'DenseNet169',
# 2: 'DenseNet201',
# 3: 'EfficientNetB0',
# 4: 'EfficientNetB1',
# 5: 'EfficientNetB2',
# 6: 'EfficientNetB3',
# 7: 'EfficientNetB4',
# 8: 'EfficientNetB5',
# 9: 'EfficientNetB6',
# 10: 'EfficientNetB7',
# 11: 'EfficientNetV2B0',
# 12: 'EfficientNetV2B1',
# 13: 'EfficientNetV2B2',
# 14: 'EfficientNetV2B3',
# 15: 'EfficientNetV2L',
# 16: 'EfficientNetV2M',
# 17: 'EfficientNetV2S',
# 18: 'InceptionResNetV2',
# 19: 'InceptionV3',
# 20: 'MobileNet',
# 21: 'MobileNetV2',
# 22: 'MobileNetV3Large',
# 23: 'MobileNetV3Small',
# 24: 'NASNetLarge',
# 25: 'NASNetMobile',
# 26: 'ResNet101',
# 27: 'ResNet101V2',
# 28: 'ResNet152',
# 29: 'ResNet152V2',
# 30: 'ResNet50',
# 31: 'ResNet50V2',
# 32: 'VGG16',
# 33: 'VGG19',
# 34: 'Xception'

# selected: 23:'MobileNetV3Small', 14:'EfficientNetV2B3', 101:'ConvNeXtXL'

# ## Prepare the data

# Model / data parameters

# imodel = int(argv[1])
# n_train = 200
# n_test = 0
# nside = 128
# n_class = 6
# batch_size = 8
# epochs = 10

print(tf.config.list_physical_devices(
    device_type=None
))

# ## Build the model
def build_model(imodel,shape, n_class, n_latent=16,comp=True):
    tf.keras.backend.clear_session()

    if imodel>=100 and imodel<103:
        from sys import path
        path.insert(0,'./ConvNeXt-TF/')
        from models.convnext_tf import get_convnext_model
        if imodel==100: 
            mdl = 'Convnext_Base'
            model = get_convnext_model(input_shape=shape,num_classes=2,depths=[3, 3, 27, 3],dims=[128, 256, 512, 1024])
        if imodel==101:
            mdl = 'Convnext_XL'
            model = get_convnext_model(input_shape=shape,num_classes=2,depths=[3, 3, 27, 3],dims=[256, 512, 1024, 2048])
        encoder = ''

    else:
        Model_list = [i for i in dir(applications) if i[0].isupper()]
        
        if imodel>len(Model_list):
            for i,j in enumerate(Model_list): print(i,j)
            assert 0,'Out of range!'
        
    #     for mdl in Model_list:
    #         print(mdl)
    #         exec('from tensorflow.keras.applications import '+mdl+' as Model')
        mdl = Model_list[imodel]
        exec('from tensorflow.keras.applications import '+mdl+' as Model', globals())
    #     Model = __import__('tensorflow.keras.applications.'+mdl)
        print(mdl)

        if shape[2]==3:
            baseModel = Model(weights="imagenet", include_top=False,
                                                       input_tensor=tf.keras.layers.Input(shape=shape))

        #     baseModel = tf.keras.applications.MobileNetV3Small(alpha=1.0, minimalistic=True,
        #                                                        weights="imagenet", include_top=False,
        #                                                        input_tensor=tf.keras.layers.Input(shape=shape))


            # show a summary of the base model
            print("[INFO] summary for base model...")
            #     print(baseModel.summary())
            inputs = baseModel.input
            headModel = baseModel.output
        else:
            inputs = layers.Input(shape=shape, name="img")
            baseModel = Model(weights="imagenet", include_top=False,
                                                       input_tensor=tf.keras.layers.Input(shape=(shape[0],shape[1],3)))

        #     baseModel = tf.keras.applications.MobileNetV3Small(alpha=1.0, minimalistic=True,
        #                                                        weights="imagenet", include_top=False,
        #                                                        input_tensor=tf.keras.layers.Input(shape=shape))


            # show a summary of the base model
            print("[INFO] summary for base model...")
            #     print(baseModel.summary())
            xh = layers.Conv2D(3, (1,1), activation=None)(inputs)
            headModel = baseModel(xh)
        
        headModel = layers.Conv2D(32, 3, activation="relu",padding='same')(headModel)
        headModel = tf.keras.layers.GlobalAveragePooling2D()(headModel)
    #     headModel = tf.keras.layers.Flatten(name="flatten")(headModel)
        encoded = tf.keras.layers.Dense(n_latent, activation="relu")(headModel)
        xl = tf.keras.layers.Dropout(0.5)(encoded)
        output = tf.keras.layers.Dense(n_class, activation="softmax")(xl)
        # place the head FC model on top of the base model (this will become
        # the actual model we will train)
        encoder = keras.models.Model(inputs=inputs, outputs=encoded)
        model = keras.models.Model(inputs=inputs, outputs=output,name=mdl)
    
    if comp:
        lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(initial_learning_rate=1e-5,
                                                                     decay_steps=10,
                                                                     decay_rate=0.95)
        opt = tf.keras.optimizers.Adam(learning_rate=lr_schedule)
        #opt = tf.keras.optimizers.Adagrad(learning_rate=lr_schedule)
        #opt = tf.keras.optimizers.RMSprop(learning_rate=lr_schedule)
#         loss = tf.keras.losses.CategoricalCrossentropy()
        loss = tf.keras.losses.CategoricalCrossentropy()
    
        model.compile(loss=loss, optimizer=opt,metrics=["accuracy"])
    
#         model.compile(optimizer=Adam(0.001), loss='binary_crossentropy', metrics=['acc'])
    model.summary()
    return model,encoder,mdl

print('[INFO] building neural net...')



import argparse
# python --imodel $imodel --dsize $dsize --nside $nside --n_class $n_class --batch_size $bs --epochs $epochs --mode $ mode
parser = argparse.ArgumentParser()
parser.add_argument("--imodel", default=100, type=int, help="model number")
parser.add_argument("--dsize", default=20000, type=int, help="data size")
parser.add_argument("--nside", default=256, type=int, help="number of side in each pach")
parser.add_argument("--n_class", default=2, type=int, help="Number of classes")
parser.add_argument("--batch_size", default=64, type=int, help="batch size")
parser.add_argument("--epochs", default=100, type=int, help="training epochs")
parser.add_argument("--mode", default='train', type=str, help="mode can be train or test")
parser.add_argument("--ntry", default=0, type=int, help="number of trial")

args = parser.parse_args()

imodel = args.imodel
dsize = args.dsize
nside = args.nside
n_class = args.n_class
batch_size = args.batch_size
epochs = args.epochs
mode = args.mode
ntry = args.ntry
prefix = 'logs_cls'
prefix = 'logs_cls_Quadro_6000'

if imodel==10 or imodel==100 or imodel==15: batch_size=32
if imodel==101: batch_size=16

if mode=='train':
    fname = '{}_{}_nd{}_ns{}_nc{}_bs{}_ep{}_{}'.format(imodel,mode,dsize,nside,n_class,batch_size,epochs,ntry)
    if exists(fname):
        print('Result exists!')
        exit()
    
    # x_train, y_train = np.random.normal(0,1,(dsize,nside,nside,3)),np.random.randint(0,n_class,dsize)
    x_train, y_train = np.random.normal(0,255,(dsize,nside,nside,3)),np.random.randint(0,n_class,dsize)
    trshs = np.linspace(10,240,n_class)
    for i in range(n_class):
        thsr = trshs[i]
        filt = y_train==i
        xp = x_train[filt]
        filt2 = xp<thsr
        xp[filt2] = 0
        x_train[filt] = xp
    x_train = x_train.astype(np.uint8)
    y_train = keras.utils.to_categorical(y_train, n_class)
    d1_train = tf.data.Dataset.from_tensor_slices(x_train)
    d2_train = tf.data.Dataset.from_tensor_slices(y_train)
    d_train = tf.data.Dataset.zip((d1_train,d2_train))

    transforms = Compose([
                Rotate(limit=40),
                RandomBrightnessContrast(brightness_limit=0.1,contrast_limit=0.2,p=0.5),
                ImageCompression(quality_lower=85, quality_upper=100, p=0.5),
                HueSaturationValue(hue_shift_limit=20, sat_shift_limit=30, val_shift_limit=20, p=0.5),
                # RandomContrast(limit=0.2, p=0.5),
                HorizontalFlip(),
            ])
    def aug_fn(image, img_size):
        data = {"image":image}
        aug_data = transforms(**data)
        aug_img = aug_data["image"]
        aug_img = tf.cast(aug_img/255.0, tf.float32)
        return aug_img
    def process_data(image, label, img_size):
        aug_img = tf.numpy_function(func=aug_fn, inp=[image, img_size], Tout=tf.float32)
        return aug_img, label
    d_train = d_train.map(partial(process_data, img_size=120),
                         num_parallel_calls=AUTOTUNE).prefetch(AUTOTUNE)
    d_train = d_train.batch(batch_size).prefetch(AUTOTUNE)
    
    input_shape = x_train.shape[1:]

    model,encoder,model_name = build_model(imodel,shape=input_shape, n_class=n_class, n_latent=64,comp=True)
    
    lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(initial_learning_rate=1e-5,
                                                                 decay_steps=4,
                                                                 decay_rate=0.95)
    opt = tf.keras.optimizers.Adam(learning_rate=lr_schedule)
    # opt = "adam"
    model.compile(loss="categorical_crossentropy", optimizer=opt, metrics=["accuracy"])

    tracker = ImpactTracker(f'{prefix}/'+fname)
    tracker.launch_impact_monitor()

    model.fit(d_train, epochs=epochs)

    tracker.stop()
    del tracker

    from experiment_impact_tracker.data_interface import DataInterface
    data_interface = DataInterface([f'{prefix}/'+fname])

    total_power = data_interface.total_power
    kg_carbon = data_interface.kg_carbon
    PUE = data_interface.PUE
    total_wall_clock_time = data_interface.exp_len_hours
    # print(total_power,kg_carbon,PUE,total_wall_clock_time)

    np.save('res/trash_cls/'+fname,[total_power,kg_carbon,total_wall_clock_time])
elif mode=='test':
    if imodel==23: dsize=4000
    
    fname = '{}_{}_nd{}_ns{}_nc{}_bs{}_{}'.format(imodel,mode,dsize,nside,n_class,batch_size,ntry)
    if exists(fname):
        print('Result exists!')
        exit()
    
    x_obs, y_obs = np.random.normal(0,1,(dsize,nside,nside,3)),np.random.randint(0,n_class,dsize)

    input_shape = x_obs.shape[1:]

    model,encoder,model_name = build_model(imodel,shape=input_shape, n_class=n_class, n_latent=64,comp=True)
    model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])
    # model.summary()
    tracker = ImpactTracker(f'{prefix}/'+fname)
    tracker.launch_impact_monitor()

    for i in range(dsize//batch_size):
        print(i,x_obs[i:i+batch_size].shape,end='\r')
        pred = model.predict(x_obs[i:i+batch_size])

    tracker.stop()
    del tracker

    from experiment_impact_tracker.data_interface import DataInterface
    data_interface = DataInterface([f'{prefix}/'+fname])

    total_power = data_interface.total_power
    kg_carbon = data_interface.kg_carbon
    PUE = data_interface.PUE
    total_wall_clock_time = data_interface.exp_len_hours
    print(total_power,kg_carbon,PUE,total_wall_clock_time)

    np.save('res/trash_cls/'+fname,[total_power,kg_carbon,total_wall_clock_time])
else:
    print('Unknown mode!')
    exit()


exit()

powers = []
emits = []
wts = []

vals = [100,200,400,1000]

for n_train in vals:
    
    # ## Train the model
    # the data, split between train and test sets
    x_train, y_train = np.random.normal(0,1,(n_train,nside,nside,3)),np.random.randint(0,n_class,n_train)
    x_test, y_test = np.random.normal(0,1,(n_test,nside,nside,3)),np.random.randint(0,n_class,n_test)

    input_shape = x_train.shape[1:]

    # convert class vectors to binary class matrices
    y_train = keras.utils.to_categorical(y_train, n_class)
    y_test = keras.utils.to_categorical(y_test, n_class)
    
    model,encoder,model_name = build_model(imodel,shape=input_shape, n_class=n_class, n_latent=64,comp=True)
    model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])
    
    tracker = ImpactTracker('logs/log'+model_name)
    tracker.launch_impact_monitor()

    model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs)

    tracker.stop()
    del tracker

    from experiment_impact_tracker.data_interface import DataInterface
    data_interface = DataInterface(['logs/log'+model_name])

    total_power = data_interface.total_power
    kg_carbon = data_interface.kg_carbon
    PUE = data_interface.PUE
    total_wall_clock_time = data_interface.exp_len_hours
    # print(total_power,kg_carbon,PUE,total_wall_clock_time)
    powers.append(total_power)
    emits.append(kg_carbon)
    wts.append(total_wall_clock_time)
    del data_interface

plt.plot(vals,powers)
plt.savefig('powers.jpg')
plt.close()
plt.plot(vals,emits)
plt.savefig('emits.jpg')
plt.close()
plt.plot(vals,wts)
plt.savefig('wts.jpg')
plt.close()
np.save([vals,powers,emits,wts],'res')