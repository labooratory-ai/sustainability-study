#!/usr/bin/env python
# coding: utf-8
from pynvml import *
nvmlInit()

free_device = -1
deviceCount = nvmlDeviceGetCount()
print('Available Devices')
for i in range(deviceCount):
    handle = nvmlDeviceGetHandleByIndex(i)
    info = nvmlDeviceGetMemoryInfo(handle)
    used = info.used/1024/1024
    if used<1024:
        print("Device", i, ":", nvmlDeviceGetName(handle))
        free_device = i
print('Device '+str(free_device)+' is selected.')
import os
os.environ["CUDA_VISIBLE_DEVICES"]=str(free_device)

import numpy as np
from cucim import CuImage
import openslide

from experiment_impact_tracker.compute_tracker import ImpactTracker

fpath = r'/path/to/files'
svs_files = [name for name in os.listdir(fpath) if name.endswith('.svs')]
svs_files_abspath = [os.path.join(fpath,i) for i in svs_files]

nside = 128
fname = str(nside)

tracker = ImpactTracker('logs_reading/'+fname)
tracker.launch_impact_monitor()

# all training procedures
n_patchs = 0
for svs_path in svs_files_abspath:
    slide_file = openslide.OpenSlide(svs_path)
    nx,ny = slide_file.dimensions

    for cx in np.arange(0,nx,nside):
        for cy in np.arange(0,ny,nside):
            image = slide_file.read_region((cx,cy), 0, (nside,nside))
            n_patchs = n_patchs+1

tracker.stop()
del tracker

from experiment_impact_tracker.data_interface import DataInterface
data_interface = DataInterface(['logs_reading/'+fname])

total_power = data_interface.total_power
kg_carbon = data_interface.kg_carbon
PUE = data_interface.PUE
total_wall_clock_time = data_interface.exp_len_hours
print(total_power,kg_carbon,PUE,total_wall_clock_time)

np.save(f'res/reading_time_{fname}',[total_power,kg_carbon,total_wall_clock_time,n_patchs])


