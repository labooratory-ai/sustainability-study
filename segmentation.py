#!/usr/bin/env python
# coding: utf-8
from pynvml import *
nvmlInit()

free_device = -1
deviceCount = nvmlDeviceGetCount()
print('Available Devices')
for i in range(deviceCount):
    handle = nvmlDeviceGetHandleByIndex(i)
    info = nvmlDeviceGetMemoryInfo(handle)
    used = info.used/1024/1024
    if used<1024:
        print("Device", i, ":", nvmlDeviceGetName(handle))
        free_device = i
print('Device '+str(free_device)+' is selected.')
import os
os.environ["CUDA_VISIBLE_DEVICES"]=str(free_device)

import numpy as np
from scipy.ndimage import gaussian_filter
from os.path import exists
from tensorflow import keras
from tensorflow.keras import layers
import tensorflow as tf
from tensorflow.keras import layers
from keras_unet_collection import models
from keras_unet_collection.losses import dice
from functools import partial
from albumentations import (
    Compose, JpegCompression, HueSaturationValue, HorizontalFlip,
    Rotate,ImageCompression,RandomBrightnessContrast
)

AUTOTUNE = tf.data.experimental.AUTOTUNE

from experiment_impact_tracker.compute_tracker import ImpactTracker

# 0: 'unet_2d',
# 1: 'vnet_2d',
# 2: 'att_unet_2d',
# 3: 'unet_plus_2d',
# 4: 'unet_3plus_2d',
# 5: 'unet_3plus_2d',
# 6: 'r2_unet_2d',
# 7: 'resunet_a_2d',
# 8: 'u2net_2d',
# 9: 'transunet_2d',
# 10: 'swin_unet_2d'

# selected: 1:'vnet_2d', 3:'unet_plus_2d', 9:'transunet_2d'

def model_maker(imodel,shape,n_class):
    if imodel==0:
        model = models.unet_2d(shape, [64, 128, 256, 512, 1024], n_labels=n_class,
                              stack_num_down=2, stack_num_up=1,
                              activation='GELU', output_activation='Softmax', 
                              batch_norm=True, pool='max', unpool='nearest', name='unet')
    if imodel==1:
        model = models.vnet_2d(shape, filter_num=[16, 32, 64, 128, 256], n_labels=n_class,
                              res_num_ini=1, res_num_max=3, 
                              activation='PReLU', output_activation='Softmax', 
                              batch_norm=True, pool=False, unpool=False, name='vnet')
    if imodel==2:
        model = models.att_unet_2d(shape, [64, 128, 256, 512], n_labels=n_class,
                                   stack_num_down=2, stack_num_up=2,
                                   activation='ReLU', atten_activation='ReLU', attention='add', output_activation=None, 
                                   batch_norm=True, pool=False, unpool='bilinear', name='attunet')
    if imodel==3:
        model = models.unet_plus_2d(shape, [64, 128, 256, 512], n_labels=n_class,
                                    stack_num_down=2, stack_num_up=2,
                                    activation='LeakyReLU', output_activation='Softmax', 
                                    batch_norm=False, pool='max', unpool=False, deep_supervision=True, name='xnet')
    if imodel==4:
        model = models.unet_3plus_2d(shape, n_labels=n_class, filter_num_down=[64, 128, 256, 512], 
                                     filter_num_skip='auto', filter_num_aggregate='auto', 
                                     stack_num_down=2, stack_num_up=1, activation='ReLU', output_activation='Sigmoid',
                                     batch_norm=True, pool='max', unpool=False, deep_supervision=True, name='unet3plus')
    if imodel==5:
        model = models.unet_3plus_2d(shape, n_labels=n_class, filter_num_down=[64, 128, 256, 512], 
                                     filter_num_skip=[64, 64, 64], filter_num_aggregate=256, 
                                     stack_num_down=2, stack_num_up=1, activation='ReLU', output_activation='Sigmoid',
                                     batch_norm=True, pool='max', unpool=False, deep_supervision=True, name='unet3plus')

    if imodel==6:
        model = models.r2_unet_2d(shape, [64, 128, 256, 512], n_labels=n_class,
                                  stack_num_down=2, stack_num_up=1, recur_num=2,
                                  activation='ReLU', output_activation='Softmax', 
                                  batch_norm=True, pool='max', unpool='nearest', name='r2unet')
    if imodel==7:
        model = models.resunet_a_2d(shape, [32, 64, 128, 256, 512, 1024], 
                                    dilation_num=[1, 3, 15, 31], 
                                    n_labels=n_class, aspp_num_down=256, aspp_num_up=128, 
                                    activation='ReLU', output_activation='Sigmoid', 
                                    batch_norm=True, pool=False, unpool='nearest', name='resunet')

    if imodel==8:
        model = models.u2net_2d(shape, n_labels=n_class, 
                                filter_num_down=[64, 128, 256, 512], filter_num_up=[64, 64, 128, 256], 
                                filter_mid_num_down=[32, 32, 64, 128], filter_mid_num_up=[16, 32, 64, 128], 
                                filter_4f_num=[512, 512], filter_4f_mid_num=[256, 256], 
                                activation='ReLU', output_activation=None, 
                                batch_norm=True, pool=False, unpool=False, deep_supervision=True, name='u2net')
    if imodel==9:
        model = models.transunet_2d(shape, filter_num=[64, 128, 256, 512], n_labels=n_class, stack_num_down=2, stack_num_up=2,
                                        embed_dim=768, num_mlp=3072, num_heads=12, num_transformer=12,
                                        activation='ReLU', mlp_activation='GELU', output_activation='Softmax', 
                                        batch_norm=True, pool=True, unpool='bilinear', name='transunet')
    if imodel==10:
        model = models.swin_unet_2d(shape, filter_num_begin=64, n_labels=n_class, depth=4, stack_num_down=2, stack_num_up=2, 
                                    patch_size=(2, 2), num_heads=[4, 8, 8, 8], window_size=[4, 2, 2, 2], num_mlp=512, 
                                    output_activation='Softmax', shift_window=True, name='swin_unet')
    return model


import argparse
# python --imodel $imodel --dsize $dsize --nside $nside --n_class $n_class --batch_size $bs --epochs $epochs --mode $ mode
parser = argparse.ArgumentParser()
parser.add_argument("--imodel", default=100, type=int, help="model number")
parser.add_argument("--dsize", default=2000, type=int, help="data size")
parser.add_argument("--nside", default=128, type=int, help="number of side in each pach")
parser.add_argument("--n_class", default=2, type=int, help="Number of classes")
parser.add_argument("--batch_size", default=64, type=int, help="batch size")
parser.add_argument("--epochs", default=100, type=int, help="training epochs")
parser.add_argument("--mode", default='train', type=str, help="mode can be train or test")
parser.add_argument("--ntry", default=0, type=int, help="number of trial")

args = parser.parse_args()

imodel = args.imodel
dsize = args.dsize
nside = args.nside
n_class = args.n_class
batch_size = args.batch_size
epochs = args.epochs
mode = args.mode
ntry = args.ntry

if imodel==7: batch_size=32
if imodel==9: batch_size=16

if mode=='train':
    fname = '{}_{}_nd{}_ns{}_nc{}_bs{}_ep{}_{}'.format(imodel,mode,dsize,nside,n_class,batch_size,epochs,ntry)
    if exists(fname):
        print('Result exists!')
        exit()
        
    # Model / data parameters
    # the data, split between train and test sets
    # x_train = np.random.normal(0,1,(dsize,nside,nside,3))
    # y_train = np.random.normal(0,1,(dsize,nside,nside,n_class))
    x_train = np.random.uniform(0,255,(dsize,nside,nside,1))
    x_train = np.concatenate(3*[x_train],axis=-1)
    x_train = gaussian_filter(x_train,1.)
    x_train = (x_train/x_train.max()*255).astype(np.uint8)
    y_train = (np.mean(x_train,axis=-1,keepdims=True)>150).astype(np.uint8)

    d1_train = tf.data.Dataset.from_tensor_slices(x_train)
    d2_train = tf.data.Dataset.from_tensor_slices(y_train)

    d_train = tf.data.Dataset.zip((d1_train,d2_train))

    transforms = Compose([
                Rotate(limit=40),
                RandomBrightnessContrast(brightness_limit=0.1,contrast_limit=0.2,p=0.5),
                ImageCompression(quality_lower=85, quality_upper=100, p=0.5),
                HueSaturationValue(hue_shift_limit=20, sat_shift_limit=30, val_shift_limit=20, p=0.5),
                HorizontalFlip(),
            ])

    def aug_seg(image, mask, img_size):
        data = {'image':image,'mask':mask}
        aug_data = transforms(**data)
        aug_img = aug_data['image']
        aug_msk = aug_data['mask']
        aug_img = tf.cast(aug_img/255.0, tf.float32)
        aug_msk = tf.cast(aug_msk/255.0, tf.float32)
        # aug_img = tf.cast(aug_img/255.0, tf.float32)
        # aug_img = tf.image.resize(aug_img, size=[img_size, img_size])
        # aug_msk = tf.image.resize(aug_msk, size=[img_size, img_size])
        return aug_img,aug_msk

    def process_data_seg(image, mask, img_size):
        aug_img,aug_msk = tf.numpy_function(func=aug_seg, inp=[image, mask, img_size], Tout=[tf.float32,tf.float32])
        return aug_img,aug_msk

    d_train = d_train.map(partial(process_data_seg, img_size=128),num_parallel_calls=AUTOTUNE).prefetch(AUTOTUNE)
    d_train = d_train.batch(batch_size).prefetch(AUTOTUNE)

    model = model_maker(imodel,(nside,nside,3),n_class)
    
    
    
    
    print(imodel,model.count_params())
    exit()
    
    # n_output = len(model.outputs)
    # n_output*[dice]
    model.compile(loss=keras.losses.BinaryCrossentropy(), optimizer="adam")

    tracker = ImpactTracker('logs_seg/'+fname)
    tracker.launch_impact_monitor()

    model.fit(d_train, epochs=epochs)

    tracker.stop()
    del tracker

    from experiment_impact_tracker.data_interface import DataInterface
    data_interface = DataInterface(['logs_seg/'+fname])

    total_power = data_interface.total_power
    kg_carbon = data_interface.kg_carbon
    PUE = data_interface.PUE
    total_wall_clock_time = data_interface.exp_len_hours
    # print(total_power,kg_carbon,PUE,total_wall_clock_time)

    np.save('res/trash_seg/'+fname,[total_power,kg_carbon,total_wall_clock_time])
elif mode=='test':
    if imodel==1: dsize=4000

    fname = '{}_{}_nd{}_ns{}_nc{}_bs{}_{}'.format(imodel,mode,dsize,nside,n_class,batch_size,ntry)
    if exists(fname):
        print('Result exists!')
        exit()
        
    x_obs, y_obs = np.random.normal(0,1,(dsize,nside,nside,3)),np.random.normal(0,1,(dsize,nside,nside,n_class))

    model = model_maker(imodel,(nside,nside,3),n_class)
    model.compile(loss="MSE", optimizer="adam", metrics=["mse"])
    
    tracker = ImpactTracker('logs_seg/'+fname)
    tracker.launch_impact_monitor()

    for i in range(dsize//batch_size):
        print(i,x_obs[i:i+batch_size].shape,end='\r')
        pred = model.predict(x_obs[i:i+batch_size])

    tracker.stop()
    del tracker

    from experiment_impact_tracker.data_interface import DataInterface
    data_interface = DataInterface(['logs_seg/'+fname])

    total_power = data_interface.total_power
    kg_carbon = data_interface.kg_carbon
    PUE = data_interface.PUE
    total_wall_clock_time = data_interface.exp_len_hours
    print(total_power,kg_carbon,PUE,total_wall_clock_time)

    np.save('res/trash_seg/'+fname,[total_power,kg_carbon,total_wall_clock_time])
else:
    print('Unknown mode!')
    exit()









